# If you have OpenSSL installed, we recommend updating
# the following line to use "https"
source 'https://rubygems.org'

gem "middleman", "~> 4.1.7"

# Live-reloading plugin
gem "middleman-livereload"

# Cross-templating language block fix for Ruby 1.8
platforms :mri_18 do
  gem "ruby18_source_location"
end

# For faster file watcher updates for people using Windows
gem "wdm", "~> 0.1.0", :platforms => [:mswin, :mingw]


#####
# General plugins

# assets
gem "middleman-sprockets", "4.0.0.rc.2"
gem "sprockets", "~> 4.0.0.beta2"

# Blog plugin
gem "middleman-blog" #, "~> 4.0.0"
#gem "middleman-blog-drafts"
#gem "middleman-blog-authors"

gem 'middleman-deploy', "~> 2.0.0.pre.alpha"

# Thumbnailer
#gem "middleman-thumbnailer", github: "nhemsley/middleman-thumbnailer"

# favicon support (favicon PNG should be 144×144)
gem "middleman-favicon-maker"

# HTML & XML parsing smarts
gem "nokogiri"
gem 'mini_portile'

# Syntax highlighting
gem "middleman-syntax"

# For feed.xml.builder
gem "builder", "~> 3.0"

# Better JSON lib
gem "oj"

# Friendly date library
gem 'chronic'

#####
# Bootstrap

# Bootstrap, as SASS
gem "bootstrap-sass"

gem "rake"
gem "sassc"


#####
# Formats

# less (css)
gem "therubyracer"
gem "less"

# asciidoctor
gem "asciidoctor"

# mediawiki
gem "wikicloth"

gem "coderay"
gem "stringex"

# Markdown
gem "kramdown"

gem 'open-uri-cached'

gem 'font-awesome-middleman'

# RSS/Atom parsing
gem "feedjira"

gem 'icalendar', '~> 1.5'

gem 'launchy'

gem 'middleman-pry'
