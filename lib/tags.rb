
def get_tags(resource)
  if resource.data.tags.is_a? String
    resource.data.tags.split(',').map(&:strip)
  else
    resource.data.tags
  end
end

def group_lookup(resource, sum)
  results = Array(get_tags(resource)).map(&:to_s)

  results.each do |k|
    sum[k] ||= []
    sum[k] << resource
  end
end

def get_all_tags
  resources
    .select { |resource| resource.data.tags }
    .each_with_object({}, &method(:group_lookup))
end

