## Monkeypatch: make mismatched dates fail gracefully, not bomb out

Middleman::Blog::BlogArticle.module_eval do
  alias_method :_date, :date

  # make mismatched dates fail gracefully, not bomb out
  def date
    _date
  rescue
    if @_date
      puts "Warning: " + $!.message
      @_date
    else
      fail $!.message
    end
  end
end
