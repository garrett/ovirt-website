#!/usr/bin/env sh

git submodule init && git submodule update

VENDOR=$(lsb_release -is)

if [ "${VENDOR}" = "Debian" ]; then
	sudo apt-get install build-essential ruby-dev libcurl4-gnutls-dev bundler nodejs patch imagemagick
	NODE_CMD=nodejs
else
	PKG_CMD=$(hash dnf 2>/dev/null && echo dnf || echo yum)
	sudo "${PKG_CMD}" install -y ruby-devel rubygems-devel gcc-c++ curl-devel rubygem-bundler patch zlib-devel ImageMagick
	NODE_CMD=node
fi

git submodule init && git submodule update
sudo yum install -y ruby-devel rubygems-devel gcc-c++ curl-devel rubygem-bundler patch zlib-devel ImageMagick

if [ -e vendor/bundle ]; then
	bundle update
else
	bundle install --path vendor/bundle
fi

# npm directory configuration is very linited, see: https://github.com/npm/npm/issues/775
[ -f vendor/package.json ] || ln -s ../package.json vendor/package.json
npm install --prefix ./vendor/

# patches
RUBY_ABI_VER=$(ruby -e 'puts RbConfig::CONFIG["ruby_version"]')
patch -N -d "vendor/bundle/ruby/${RUBY_ABI_VER}/gems/" -p0 -i "${PWD}/patches/scope_fix_haml.patch"

# build assets
${NODE_CMD} ./vendor/node_modules/modernizr/bin/modernizr --config app/assets/javascripts/modernizr/modernizr-config.json --dest app/assets/javascripts/modernizr/build

