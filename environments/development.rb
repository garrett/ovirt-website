
## If unspecified, timezone defaults to UTC
## For more TZs, see the top of:
## https://github.com/rails/rails/blob/master/activesupport/lib/active_support/values/time_zone.rb
#set :timezone, "Berlin"
#set :timezone, "America/New_York"

##
# Deployment: OpenShift
##
#
# There are two  methods for using OpenShift: rsync and git
#
# Both utilize the basic httpd cartridge at:
# http://cartreflect-claytondev.rhcloud.com/reflect?github=stefanozanella/openshift-cartridge-httpd
#
# When creating a new gear, use the URL above at the bottom of the page,
# in the entry box.
#
# If you'd like to use git via rsync (faster), use the following line to
# add your user@host contained within your git repo. (You may also use
# the entire git repo line).

#set :deployment_openshift, "12fea2b4c1b98683b3eae078@site-user.rhcloud.com"

# If you'd rather use OpenShift via git instead of rsync, comment out
# the above line and add your git URL as a 'production' remote.
#
# Example:
#    git remote add production OPENSHIFT_GIT_REMOTE_HERE
#
# (Note: This also works for non-OpenShift sites, provided the git
# remote is 'production' and is deployed to 'master')
#
# You still will deploy the same way, either method, by running:
#    bundle exec middleman deploy

###
# Deployment: rsync
##
#
# You can easily use rsync instead of OpenShift.
#
# Be sure to comment out openshift above. If you don't specify a port,
# it will default to 22 (ssh). If you do not specify a user, it will use
# the username of the person deploying. (You probably want to specify
# a username.)

#set :deployment_rsync, "ssh://foo@bar.com:99/some/path/here/"

