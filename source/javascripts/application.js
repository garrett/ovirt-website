//= require jquery
//= require moment
//= require fullcalendar
//= require string_score
//= require_tree ./lib

/* Turn off parts of bootstrap that you do not need in
 * _bootstrap-includes.js */
//= require _bootstrap-includes

//= require bootstrap-sortable
