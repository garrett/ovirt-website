# overload default MiddleMan helpers to adapt them to our needs

module DefaultHelpersImproved
  # Overload image_tag to add missing image support
  # (and look for space-to-underscore conversions like MediaWiki)
  def image_tag(path, params = {})
    unless path.include?('://')
      real_path = path
      real_path = File.join(config[:images_dir], real_path) unless real_path.start_with?('/')
      full_path = File.join(root, real_path)
      filename  = File.basename(path)

      # Try harder. (Look all over the resources)
      unless File.exist?(full_path)
        sitemap.resources.each do |resource|
          p = resource.path
          result = p.match(/#{filename}|#{filename.gsub(/ /, '_')}$/i)

          # Try even harder. (Look for similar filenames; handles conversions)
          unless result
            noext = filename.chomp(File.extname(filename))
            exts = 'png|gif|jpg|jpeg|svg'
            result = p.match(/(#{noext}|#{noext.gsub(/ /, '_')})\.(#{exts})$/i)
          end

          if result
            # resource's `url` is user-specified; `path` is a full path
            path = resource.url
	    break
          end
        end # loop over resources
      end # Try harder
    end

    super(path, params)
  end

  # WIP!!!

  # Overload Middleman's link_to to add missing page support
  # (and search MediaWiki imported files)
  def link_to(*args, &block)
    begin
      url_index = block_given? ? 0 : 1
      url = args[url_index]
      current_file = current_page.nil? ? nil : current_page.source_file.gsub("#{app.source_dir}/", '')

      # Strip site referential links
      if config[:links_strip_regex] && args[url_index].respond_to?('gsub!')
        args[url_index].gsub!(Regexp.new(config[:links_strip_regex]), '')
      end

      if url.respond_to?('gsub') && url.respond_to?('match') && !url.match(/^http|^#/)
        p args if url.match(/Special:/)

        if url.match(/^(Special:|User:)/i)
          # puts "WARNING: #{current_file}: Invalid link to '#{args[1]}'"
          return "<span class='broken-link link-mediawiki' data-href='#{url}' title='Special MediaWiki link: original pointed to: #{url}'>#{args.first}</span>"
        end

        url_extra = ''

        match = sitemap.resources.select do |resource|
          extra = /[#\?].*/
          url_extra = url.match(extra)
          url_fixed = url.gsub(/_/, ' ').gsub(extra, '')
          resource.data.wiki_title.to_s.downcase.strip == url_fixed.gsub(/_/, ' ').downcase.strip
        end.sort_by { |r| File.stat(r.source_file).size }.reverse.first

        args[url_index] = match.url + url_extra.to_s if match
      end

      result = super(*args, &block)

    rescue
      puts "ERROR: #{current_file || "<unknown_file>"}: Issue with link to '#{args[1]}': " + $!.message
      return "<span class='broken-link link-error' data-href='#{url}' title='Broken link: original pointed to: #{url}'>#{args.first}</span>"
    end

    result
  end
end
