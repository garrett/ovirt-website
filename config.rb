###
# Site settings
###

# Look in data/site.yml for general site configuration

Time.zone = config[:timezone] || 'UTC'

# Automatic image dimensions on image_tag helper
activate :automatic_image_sizes

# Syntax highlighting
activate :syntax

# Let's use sprockets! (It was implied before; now it needs to be
# manually turned on)
activate :sprockets

# Make URLs relative
set :relative_links, true

set :index_file, "index.html"

set :links_strip_regex, "https?:\/\/(www.)?ovirt.org\/"

# Set HAML to render HTML5 by default (when unspecified)
# It's important HAML outputs "ugly" HTML to not mess with code blocks
set :haml, format: :html5, ugly: true

# Set Markdown features for Kramdown
# (So our version of Markdown resembles GitHub's w/ other nice stuff)
set :markdown,
    transliterated_header_ids: true,
    parse_block_html: true,
    parse_span_html: true,
    tables: true,
    hard_wrap: false,
    input: 'GFM' # add in some GitHub-flavor (``` for fenced code blocks)

set :markdown_engine, :kramdown

set :asciidoc_attributes, %w(source-highlighter=coderay imagesdir=images)

set :asciidoctor,
    toc: true,
    numbered: true

set :debug_assets, true
#set :show_exceptions, false

#ready do
  #sprockets.append_path File.join root, 'app/assets'
#end

# Set directories
set :css_dir, 'stylesheets'
set :fonts_dir, 'stylesheets/fonts'
set :js_dir, 'javascripts'
set :images_dir, 'images'

#set :sprockets_imported_asset_path, '../assets/'

set :automatic_image_sizes, false

###
# Blog settings
###

activate :blog do |blog|
  blog.publish_future_dated = true

  blog.prefix = 'blog/'
  blog.layout = 'post'
  blog.tag_template = 'tag.html'
  blog.calendar_template = 'calendar.html'
  blog.default_extension = '.html.md'

  blog.sources = ':year-:month-:day-:title.html'
  # blog.permalink = ":year/:month/:day/:title.html"
  blog.permalink = ':year/:month/:title.html'
  blog.year_link = ':year.html'
  blog.month_link = ':year/:month.html'
  # blog.day_link = ":year/:month/:day.html"

  blog.generate_day_pages = false

  blog.taglink = 'tag/:tag.html'

  # blog.summary_separator = /(READMORE)/
  # blog.summary_length = 99999

  blog.paginate = true
  blog.per_page = 10
  blog.page_link = 'page=:num'
end

# activate :authors
# activate :drafts

# Enable blog layout for all blog pages
page '/blog.html', :layout => 'post'
page '/blog/*', :layout => 'post'

# Make pretty URLs
activate :directory_indexes

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout

page "/feature/*", :layout => 'feature'
page "/md/source/feature/*", :layout => 'feature' # Temporary dir

# Don't make these URLs have pretty URLs
page '/404.html', directory_index: false
page '/.htaccess', directory_index: false

# Proxy pages (http://middlemanapp.com/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

require 'lib/tags'

ready do
  # Add yearly calendar pages
  data.events.each do |year, data|
    next unless year.match(/[0-9]{4}/)
    proxy "/events/#{year}/index.html", "events/index.html", locals: {year: year}
  end

  # Add author pages
  sitemap.resources.group_by { |p| p.data['author'] }.each do |author, pages|
    next unless author

    proxy "/blog/author/#{author.parameterize.downcase}.html",
          'author.html',
          locals: { author: author, pages: pages },
          ignore: true
  end

  proxy '/blog/author.html', 'author.html', ignore: true

  # Add blog feeds
  get_all_tags.each do |tag_name, _tag_data|
    next unless tag_name

    proxy "/blog/tag/#{tag_name.downcase}.xml",
          'feed.xml',
          locals: { tag_name: tag_name },
          layout: false,
          ignore: true
  end

  proxy '/blog/feed.xml', 'feed.xml', ignore: true
  proxy '/blog/tag/index.html', 'tag.html', ignore: true

  # Auto-add index.html.md pages where they are lacking
  Dir.glob('source/**/').each do |path|
    next if Dir.glob("#{path}index.*").count > 0
    next if /source\/(images|stylesheets|javascripts|fonts|blog)/.match path

    path_url = path.sub('source', '')

    proxy "#{path_url}index.html",
          'indexless.html',
          locals: { path: path_url },
          ignore: true
  end
end

###
# Monkey patches
###

require 'lib/monkeypatch_blog_date'


###
# Development-only configuration
###
#
configure :development do
  puts "\nUpdating git submodules..."
  puts `git submodule init && git submodule sync`
  puts `git submodule foreach "git pull -qf origin master"`
  `git commit -q -m "updated events" data/events &>/dev/null`
  puts "\n"
  puts '== Administration is at http://0.0.0.0:4567/admin/'

  # activate :livereload
  # config.sass_options = {:debug_info => true}
  # config.sass_options = {:line_comments => true}
end

# Build-specific configuration
configure :build do
  puts "\nUpdating git submodules..."
  puts `git submodule init`
  puts `git submodule foreach "git pull -qf origin master"`
  puts "\n"

  ## Ignore administration UI
  ignore '/admin/*'
  ignore '/javascripts/admin*'
  ignore '/stylesheets/lib/admin*'

  ## Ignore Gimp source files
  ignore 'images/*.xcf*'

  # Don't export source JS
  ignore 'javascripts/vendor/*'
  ignore 'javascripts/lib/*'

  # Don't export source CSS
  ignore 'stylesheets/vendor/*'
  ignore 'stylesheets/lib/*'

  ignore 'events-yaml*'

  # Minify JavaScript and CSS on build
  activate :minify_javascript
  activate :minify_css
  # activate :gzip

  # Force a browser reload for new content by using
  # asset_hash or cache buster (but not both)
  activate :cache_buster
  # activate :asset_hash

  # Use relative URLs for all assets
  # activate :relative_assets

  # Compress PNGs after build
  # First: gem install middleman-smusher
  # require "middleman-smusher"
  # activate :smusher

  # Or use a different image path
  # set :http_path, "/Content/images/"

  # Favicon PNG should be 144×144 and in source/images/favicon_base.png
  # Note: You need ImageMagick installed for favicon_maker to work
  activate :favicon_maker do |f|
    f.template_dir  = File.join(root, 'source', 'images')
    f.output_dir    = File.join(root, 'build', 'images')
    f.icons = {
      'favicon_base.png' => [
        { icon: 'favicon.png', size: '16x16' },
        { icon: 'favicon.ico', size: '64x64,32x32,24x24,16x16' }
      ]
    }
  end
end

after_configuration do
  # sprockets.append_path "#{root}/derp/javascripts/"
  sprockets.append_path "app/assets/"
end

###
# Deployment
##

if config[:deployment_openshift]
  os_token, os_host = config[:deployment_openshift].match(/([0-9a-f]+)@([^\/]+)/).captures

  deploy_config = {
    deploy_method: :rsync,
    user: os_token,
    host: os_host,
    path: "/var/lib/openshift/#{os_token}/app-root/repo/public",
    clean: true, # remove orphaned files on remote host
    build_before: true # default false
  }

elsif config[:deployment_rsync]
  rsync = URI.parse(config[:deployment_rsync])

  deploy_config = {
    deploy_method: :rsync,
    user: rsync.user || ENV[:USER],
    host: rsync.host,
    path: rsync.path,
    port: rsync.port || 22,
    clean: true, # remove orphaned files on remote host
    build_before: true # default false
  }

else
  # For OpenShift,
  #
  # 1) use the barebones httpd cartridge from:
  #    http://cartreflect-claytondev.rhcloud.com/reflect?github=stefanozanella/openshift-cartridge-httpd
  #    (Add as URL at the bottom of the create from cartridge page)
  #
  # 2) Copy your new site's git repo URL and use it for 'production':
  #    git remote add production OPENSHIFT_GIT_REMOTE_HERE
  #
  # 3) Now, you can easily deploy to your new OpenShift site!
  #    bundle exec middleman deploy

  deploy_config = {
    deploy_method: :git,
    remote: 'production',
    branch: 'master',
    build_before: true # default false
  }
end

activate :deploy, deploy_config

